<?php

require 'BookInterface.php';
class Book implements BookInterface
{
    private $currentPage = 0;
    private $pages = [];

    public function __construct(array $pages)
    {
        $this->pages = $pages;
    }

    public function open()
    {
        $this->currentPage = 1;
    }

    public function turnPage(): void
    {
        error_log('Page: ' . $this->currentPage, 0);
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function getCurrentContent(): string
    {
        if ($this->currentPage > 0 && $this->currentPage <= count($this->pages)) {
            return $this->pages[$this->currentPage - 1];
        }
        return "";
    }

}