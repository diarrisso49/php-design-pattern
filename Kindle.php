<?php

class Kindle implements eReaderInterface
{

    private $currentPage = 0;
    private $pages = [
        "Page 1: E-book Introduction",
        "Page 2: E-book Chapter 1",
        "Page 3: E-book Chapter 2",
        "Page 4: E-book Conclusion"
    ];

    public function open()
    {
    error_log(' person Opening the e-book.', 0);
    }

    public function pressNextButton(): void
    {
        error_log(' person turns the page.', 0);
        if ($this->currentPage < count($this->pages)) {

            echo "Page: " . $this->currentPage . "\n";
            $this->currentPage++;
        }
    }
}