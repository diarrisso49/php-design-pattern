<?php


require_once 'BookInterface.php';
require_once 'eReaderInterface.php';
class eReadAdapter implements BookInterface
{

    public function __construct( private eReaderInterface $reader)
    {

    }
    public function open()
    {
       return $this->reader->open();
    }

    public function turnPage()
    {
        return $this->reader->pressNextButton();
    }


}