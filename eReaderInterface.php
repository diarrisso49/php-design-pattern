<?php

interface eReaderInterface
{
    public function open();
    public function pressNextButton();

}