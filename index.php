<?php

require 'Book.php';
require 'Person.php';
require 'Kindle.php';
require 'eReadAdapter.php';



$dummyPages = [
    "Page 1 => Introduction",
    "Page 2 => Chapitre 1",
    "Page 3 =>  Chapitre 2",
    "Page 4 => Conclusion"
];

$dummyBook = new Book($dummyPages);


$kindle = new Kindle();

$newPerson = new Person();

$newPerson->read(new Book((array)$dummyBook));
$newPerson->read(new eReadAdapter($kindle));

error_log('Person reads book.', 0);





